import {API_LOGIN, API_URL, API_KEY,} from "./constant";

const headerApiKey = new Headers();

headerApiKey.append("Accept", "application/json");
headerApiKey.append("Content-Type", "application/json");
headerApiKey.append("apikey", `${API_KEY}`);

const onLogin = (values) => {
    return fetch(API_URL + API_LOGIN, {
      method: "POST",
      headers: headerApiKey,
      body: JSON.stringify(values),
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        return data;
      })
      .catch((err) => {
        console.log(err);
      });
  };

export {
    onLogin
}
  